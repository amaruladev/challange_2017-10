package leixx.tmdb_browser.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import leixx.tmdb_browser.R;
import leixx.tmdb_browser.containers.TMDBItemScrollListener;
import leixx.tmdb_browser.containers.TMDbItemRecyclerViewAdapter;
import leixx.tmdb_browser.structures.TMDbAPIListQuerySpecification;
import leixx.tmdb_browser.structures.TMDbItem;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class MovieFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    /**
     * Specifies the API query that is used in the view of this fragment.
     *
     * Setting the specification changes the API query and therefore resets the adapter.
     */
    private TMDbAPIListQuerySpecification specification;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MovieFragment() {
    }

    @SuppressWarnings("unused")
    public static MovieFragment newInstance(TMDbAPIListQuerySpecification specification, int columnCount) {
        MovieFragment fragment = new MovieFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        fragment.specification = specification;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setUserVisibleHint(false);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        RelativeLayout itemLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_list_layout, container, false);

        // attach adapter to RecyclerView to store TMDb Items.
        RecyclerView recyclerView = itemLayout.findViewById(R.id.movie_list_in_layout);
        Context context = recyclerView.getContext();

        recyclerView.setAdapter(new TMDbItemRecyclerViewAdapter(mListener, specification));

        LinearLayoutManager layoutManager;
        if (mColumnCount <= 1)
            layoutManager = new LinearLayoutManager(context);
        else
            layoutManager = new GridLayoutManager(context, mColumnCount);

        TMDBItemScrollListener TMDBItemScrollListener = new TMDBItemScrollListener(layoutManager);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(TMDBItemScrollListener);
        TMDBItemScrollListener.initialize(recyclerView); // loadNewItems loads initial data to adapter


        // create button for sorting the results - Popular, Top Rated, Now Playing
        Button button = itemLayout.findViewById(R.id.sort_button);
        button.setOnClickListener(new SortDialogClickListener(recyclerView, TMDBItemScrollListener));
        return itemLayout;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setSpecification(TMDbAPIListQuerySpecification specification) {
        this.specification = specification;
    }

    public TMDbAPIListQuerySpecification getSpecification() {
        return this.specification;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(TMDbItem item);
    }

    // TODO this is rushed/spaghetti, create cleaner implementation with better variable access/storage
    // TODO is passing the Adapter to View.OnClickListener a correct implementation?
    // TODO maybe pagination should be set inside the adapter and just leave out TMDBItemScrollListener out of the equation
    private class SortDialogClickListener implements View.OnClickListener {

        // reset the contents of this recycleView's adapter on button click
        RecyclerView recyclerView;
        TMDBItemScrollListener viewScrollListener;
        SortDialogClickListener(RecyclerView recyclerView, TMDBItemScrollListener viewScrollListener) {
            this.recyclerView = recyclerView;
            this.viewScrollListener = viewScrollListener;

        }
        @Override
        public void onClick(View v) {
            // TODO should this prompt be an AlertDialog? Is this an "Alert"?
            // Build an AlertDialog
            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());

            // String array for alert dialog single choice items
            String[] options = {
                    "Popular", // 0
                    "Top Rated", // 1
                    "Now Playing" // 2 // this action is specific for movies
            };

            /*
              options -> choices available in the single choice dialog
              specification.getActionID() -> sort criterion that was already applied
             */
            builder.setSingleChoiceItems(options, specification.getActionID(), new DialogInterface.OnClickListener() {

                // handle user clicking on one of the sort dialog options
                @Override
                public void onClick(DialogInterface dialog, int clickedId) {
                    dialog.dismiss();
                    /// / if the sort criterion changes, reset the adapter's contents,
                    // then reset the pagination in TMDBItemScrollListener
                    // and initialize it again (the new sort criterion will be taken into effect)
                    int prevId= specification.getActionID();
                    if (prevId != clickedId) {
                        specification.setAction(clickedId);
                        TMDbItemRecyclerViewAdapter adapter = (TMDbItemRecyclerViewAdapter) recyclerView.getAdapter();
                        adapter.resetContents();
                        viewScrollListener.initialize(recyclerView);
                    }
                }

            });

            AlertDialog dialog = builder.create();
            // Display the alert dialog on interface
            dialog.show();
        }
    }
}
