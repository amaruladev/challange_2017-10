package leixx.tmdb_browser.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;

import java.util.Arrays;
import java.util.List;

/**
 * Created by leixx on 10/29/17.
 */

public class SortDialogClickListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        // TODO should this prompt be an AlertDialog? Is this an "Alert"?
        // Build an AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        final int checkedItem = 0;

        builder.setTitle("Sort by:");
        // String array for alert dialog multi choice items
        String[] options = {
                "Popular",
                "Top Rated",
                "Now Playing"
        };

        builder.setSingleChoiceItems(options, checkedItem, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
            }

        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();
    }
}
