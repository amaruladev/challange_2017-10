package leixx.tmdb_browser.fragments;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

/**
 * Created by leixx on 10/26/17.
 *
 * used and edited https://gist.github.com/nesquena/d09dc68ff07e845cc622
 */

/**
 * Checks whether new data has to be loaded on endless scrolling.
 * Implement your onLoadMore methods to query the API and add data to RecyclerView's adapter
 */
public abstract class EndlessRecyclerViewScrollListener extends RecyclerView.OnScrollListener {
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 5;

    // The current offset index of data you have loaded
    protected int currentPage = 1;

    // The number of items loaded in the dataset after the last loadNewItems
    protected int previousLoadedItemCount = 0;

    // True if we are still waiting for the last set of data to loadNewItems.
    private boolean loading = true;

    // Sets the starting page index
    private int startingPageIndex = 1;


    RecyclerView.LayoutManager mLayoutManager;

    public EndlessRecyclerViewScrollListener(LinearLayoutManager layoutManager) {
        this.mLayoutManager = layoutManager;
    }

    public EndlessRecyclerViewScrollListener(GridLayoutManager layoutManager) {
        this.mLayoutManager = layoutManager;
        visibleThreshold = visibleThreshold * layoutManager.getSpanCount();
    }

    public EndlessRecyclerViewScrollListener(StaggeredGridLayoutManager layoutManager) {
        this.mLayoutManager = layoutManager;
        visibleThreshold = visibleThreshold * layoutManager.getSpanCount();
    }

    public int getLastVisibleItem(int[] lastVisibleItemPositions) {
        int maxSize = 0;
        for (int i = 0; i < lastVisibleItemPositions.length; i++) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i];
            }
            else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i];
            }
        }
        return maxSize;
    }

    // This happens many times a second during a scroll, so be wary of the code you place here.
    // We are given a few useful parameters to help us work out if we need to loadNewItems some more data,
    // but first we check if we are waiting for the previous loadNewItems to finish.
    @Override
    public void onScrolled(RecyclerView view, int dx, int dy) {
        // in case the user is not scrolling down don't continue
        if (dy <= 0)
            return;

        int lastVisibleItemPosition = 0;
        int loadedItemCount = mLayoutManager.getItemCount();

        if (mLayoutManager instanceof StaggeredGridLayoutManager) {
            int[] lastVisibleItemPositions = ((StaggeredGridLayoutManager) mLayoutManager).findLastVisibleItemPositions(null);
            // get maximum element within the list
            lastVisibleItemPosition = getLastVisibleItem(lastVisibleItemPositions);
        } else if (mLayoutManager instanceof GridLayoutManager) {
            lastVisibleItemPosition = ((GridLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        } else if (mLayoutManager instanceof LinearLayoutManager) {
            lastVisibleItemPosition = ((LinearLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        }

        // If the loaded item count is zero and the previous isn't, assume the
        // list is invalidated and should be reset back to initial state
        if (loadedItemCount < previousLoadedItemCount) {
            this.currentPage = this.startingPageIndex;
            this.previousLoadedItemCount = loadedItemCount;
            if (loadedItemCount == 0) {
                this.loading = true;
            }
        }
        // If it’s still loading, we check to see if the dataset count has
        // changed, if so we conclude it has finished loading and update the current page
        // number and loaded item count.
        if (loading && (loadedItemCount > previousLoadedItemCount)) {
            loading = false;
            previousLoadedItemCount = loadedItemCount;
        }

        // If it isn’t currently loading, we check to see if we have breached
        // the visibleThreshold and need to reload more data.
        // If we do need to reload some more data, we execute onLoadMore to fetch the data.
        // threshold should reflect how many total columns there are too
        if (!loading && (lastVisibleItemPosition + visibleThreshold) >= loadedItemCount) {
            currentPage++;
            onLoadMore(currentPage, loadedItemCount, view);
            loading = true;
        }
    }

    // Call this method whenever performing new searches
    public void resetState() {
        this.currentPage = this.startingPageIndex;
        this.previousLoadedItemCount = 0;
        this.loading = true;
    }

    // Defines the process for actually loading more data based on page
    public abstract void onLoadMore(int page, int loadedItemsCount, RecyclerView view);

}