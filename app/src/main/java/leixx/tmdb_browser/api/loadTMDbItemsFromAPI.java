package leixx.tmdb_browser.api;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import leixx.tmdb_browser.R;
import leixx.tmdb_browser.containers.TMDbItemRecyclerViewAdapter;
import leixx.tmdb_browser.structures.TMDbItem;

/**
 * place GET requests to an API with this class.
 *
 * Use classes such as {@link leixx.tmdb_browser.structures.TMDbItem}
 * to construct an URI for querying the TMDb API.
 *
 *
 * The method doInBackground is called with 1 URL argument.
 * It places a GET request on the given URL and then constructs JSONObjects from the response.
 *
 */
class loadTMDbItemsFromAPI extends AsyncTask<URL, Void, ArrayList<TMDbItem>> {

    private TMDbItemsLoader tmDbItemsLoader;
    private WeakReference<RecyclerView> rvRef;
    private Exception exception;

    /**
     * @param tmDbItemsLoader Used to access information about the query - item counts, type
     * @param rvRef           reference to view that will be updated from this action
     */
    loadTMDbItemsFromAPI(TMDbItemsLoader tmDbItemsLoader, WeakReference<RecyclerView> rvRef) {
        this.tmDbItemsLoader = tmDbItemsLoader;
        this.rvRef = rvRef;
    }

    protected void onPreExecute() {
//        progressBar.setVisibility(View.VISIBLE);
    }

    protected ArrayList<TMDbItem> doInBackground(URL... urls) throws IllegalArgumentException {
        long begin = System.currentTimeMillis();

        if (urls.length > 1)
            throw new IllegalArgumentException("Exactly 1 URL must be entered!");

        try {
            HttpsURLConnection urlConnection = (HttpsURLConnection) urls[0].openConnection();
            try {

                // load the response into a JSONObject
                JSONObject json = CreateJSONFromInputStream(urlConnection.getInputStream());
                // access the results of the response and construct a TMDbItem from each one
                JSONArray jsonArray = (JSONArray) json.get("results");
                if (jsonArray.length() > 0) {
                    ArrayList<TMDbItem> TMDbItems = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject item = (JSONObject) jsonArray.get(i);
                        TMDbItems.add(ConstructTMDbItemFromJSON(item));
                    }
                    return TMDbItems;
                }
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            Log.e("ERROR", e.getMessage(), e);
        }
        return null;
    }


    protected void onPostExecute(ArrayList<TMDbItem> response) {
        if (response != null) {
            final RecyclerView view = rvRef.get();
            final TMDbItemRecyclerViewAdapter adapter = (TMDbItemRecyclerViewAdapter) view.getAdapter();

            for (TMDbItem item : response
                    )
                adapter.add(item);
            // notify the adapter about new items being added
            view.getAdapter().notifyItemRangeInserted(
                    tmDbItemsLoader.loadedItemsCount,
                    tmDbItemsLoader.loadedItemsCount + response.size());

//                response = "Error receiving more data";
        }
//        progressBar.setVisibilit/y(View.GONE);
//            Log.i("INFO", response);
    }

    /**
     * // TODO depending on the future scenario, TMDbItem can be split into specific objects
     *
     * construct TMDbItem with as many properties as possible
     * If properties are not available, place empty strings or 0's instead.
     * @param json json with data for one TMDbItem
     * @return Constructed TMDbItem
     */
    private TMDbItem ConstructTMDbItemFromJSON(JSONObject json) {
        int id = json.optInt("id");
        double rating = json.optDouble("vote_average");
        String title = (String) json.optString("title");
        String description = (String) json.optString("overview");

        String poster_path = (String) json.optString("poster_path");
        Uri poster_uri = createFullImageUri(poster_path.substring(1));

        // simpler solution than fiddling with Date()
        String date_string = json.optString("release_date");
        int year = 0;
        if (!date_string.isEmpty()) {
            year = Integer.parseInt(date_string.substring(0, 3));
            Log.d("YEAR", ""+year);
        }


        JSONArray genre_array = json.optJSONArray("genre_ids");
        String genres = "";
        if (genre_array != null) {
            int genre_ids[] = new int[genre_array.length()];
            for (int i = 0; i < genre_array.length(); i++)
                genre_ids[i] = genre_array.optInt(i);
            try {
                genres = getGenresFromIDs(genre_ids);
            } catch (Exception e) {
                Log.e("ERROR", "Couldn't fetch genres from API");
                e.getStackTrace();
            }
        }

        return new TMDbItem(
                id,
                title,
                description,
                "",
                genres,
                poster_uri,
                year,
                rating);
    }

    private Uri createFullImageUri(String relative_path) {
        Uri.Builder builder = new Uri.Builder();
        Context context = rvRef.get().getContext();
        builder.scheme("https")
                .authority(context.getString(R.string.CDN_AUTHORITY))
                .appendPath("t")
                .appendPath("p")
                .appendPath(context.getString(R.string.API_IMAGE_WIDTH_FULL))
                .appendPath(relative_path);
        return builder.build();
    }

    private JSONObject getAPIConfiguration() throws JSONException {
        Uri.Builder builder = new Uri.Builder();
        Context context = rvRef.get().getContext();
        builder.scheme("https")
                .authority(context.getString(R.string.API_AUTHORITY))
                .appendPath(context.getString(R.string.API_VERSION))
                .appendPath("configuration")
                .appendQueryParameter("api_key", context.getString(R.string.API_KEY));
        try {
            URL url = new URL(builder.build().toString());
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            return CreateJSONFromInputStream(urlConnection.getInputStream());
        } catch (IOException e) {
            Log.e("ERROR", e.getMessage(), e);
        }
        return null;
    }

    private JSONObject CreateJSONFromInputStream(InputStream is) throws JSONException, IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null)
            stringBuilder.append(line);
        bufferedReader.close();
        return new JSONObject(stringBuilder.toString());
    }


    private URL UriToURL(Uri uri) throws MalformedURLException {
        return new URL(uri.toString());
    }

    private String getGenresFromIDs(int[] ids) throws IOException, JSONException {
        Uri.Builder builder = new Uri.Builder();
        Context context = rvRef.get().getContext();
        builder.scheme("https")
                .authority(context.getString(R.string.API_AUTHORITY))
                .appendPath(context.getString(R.string.API_VERSION))
                .appendPath("genre")
                .appendQueryParameter("api_key", context.getString(R.string.API_KEY));

        switch (tmDbItemsLoader.getType()) {
            case MOVIE:
                builder.appendPath("movie");
                break;
            case TV:
                builder.appendPath("tv");
                break;
            default:
                return "";
        }
        builder.appendPath("list");

        URL url = UriToURL(builder.build());

        HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
        JSONObject json = CreateJSONFromInputStream(urlConnection.getInputStream());
        JSONArray genresJSONArray = json.optJSONArray("genres");

        SparseArray<String> genres = new SparseArray<>();
        for (int i = 0; i < genresJSONArray.length(); i++) {
            JSONObject item = genresJSONArray.optJSONObject(i);
            int id = item.optInt("id");
            String name = item.optString("name");
            genres.append(id, name);
        }

        StringBuilder genresBuilder = new StringBuilder();
        for (int i = 0; i < ids.length; i++) {
            genresBuilder.append(genres.get(ids[i]));
            if (i != ids.length - 1)
                genresBuilder.append(", ");
        }
        return genresBuilder.toString();
    }
}
